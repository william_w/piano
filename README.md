# piano

This app displays an interactive Piano keyboard built on React. The app logs the keys that have been pressed and displays the list of keys on a label. It will also accept a list of keys on the input text box and once validated, the keyboard will playback the list at an interval of 1 key press per second.

## Pre-requisites
You will need to have the following installed and configured appropriately on your computer.
* Nodejs
* Git
* yarn


## Setup:

### Clone the project to your local drive
1. Navigate to your projects folder.
2. Run the command `git clone git@gitlab.com:william_w/piano.git` to clone the repository.

## Test and Run:

### Initialize your project
1. Open your project folder on your preferred terminal.
2. Run the command `yarn` to install the project dependencies.

    
### Running the project

1.  Run the command `yarn test` to execute all the unit tests.
2.  On your terminal, run the command `yarn start` to launch the application; this will load the app on your default browser at the local address `http://localhost:3000`
3.  You can also open the address with your preferred browser as long as the app is running.