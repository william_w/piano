import React from 'react';
import Piano from '../components/Piano';

// To load multiple instances of the Piano component onto this container, you need to pass a unique parameter {id} to Piano
const PianoLoader = () => {
  return <Piano />;
}

export default PianoLoader;