import React, {useState, Fragment} from 'react';

const Player = ({handleChange, handlePlay}) => {
  const [errors, setErrors] = useState('');
  const [disabled, setDisabled] = useState(true);

  const validate = (letters) => {
    const regexp = /[abcdefg]/i;
    let valid = true;
    let message = '';
    letters.forEach(letter => {
      if (!letter.match(regexp) || !(letter.trim().length === 1)) {
        valid = false;
        if (!letter.match(regexp) && letter.trim() !== '')
          message = 'Please use small or capital letters [A to G] only.'

        if (letter.trim().length > 1) 
          message = message === '' ? 'Please type in only one letter seperated by a coma.' : message;
      }
    });

    if (!valid)
      setErrors(message);
    else {
      setErrors('');
    }

    return valid;
  }

  const changeHandler = e => {
    const letters = e.target.value.split(',');
    const isValid = validate(letters);

    setDisabled(isValid ? false : true);
    handleChange(e.target.value);
  }

  const submitHandler = e => {
    e.preventDefault();
    handlePlay();
  }

  return (
    <Fragment>
      <div className='player prompt'>
        Type in the piano keyboard letters, each separated by a coma (,) and click the Play button.
      </div>
      <input 
        className='player text-field' type='text' 
        name='piano-keys' onChange={changeHandler} 
        placeholder='Example: a,b,c,E,f,G' />
      <button onClick={submitHandler} disabled={disabled}>Play</button>
      <br />
      <span className='player error'>{errors}</span>
    </Fragment>
  );
}

export default Player;