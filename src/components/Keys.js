import React from 'react';

const Keys = ({pianoKey, id, handleChange}) => {
  const {color, letter} = pianoKey;

  const handleKeyClick = () => {
    handleChange(letter);
  }

  return (
    <li 
      className={`${color} ${letter}`} 
      onClick={handleKeyClick}
      id={`${id}-${letter}`}
      data-testid={`li-${letter}`}
    >
      <div>{letter.toUpperCase()}</div>
    </li>
  );
}

export default Keys;