import React from 'react';

const Logger = ({logs}) => {
  return (
    <label data-testid='log-label'>
      Log: {logs.join(' ')}
    </label>);
}

export default Logger;