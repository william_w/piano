import React, {useState} from 'react';
import {pianoKeys} from '../constants';
import Keys from './Keys';
import Logger from './Logger';
import Player from './Player';

import '../css/style.css';


/**
 * This component holds all the UI elements of the piano. It tracks the state of a
 * piano instance including a log of the pressed keys and a list of key letters that
 * should be played.
 * The id paramenter enables the component to distinguish between separate instances
 * of the pioano element
 * @param {number} id 
 */
const Piano = ({id=0}) => {
  const [logs, setLogs] = useState([]); // holds a list of letters representing the keys that have been pressed
  const [letters, setLetters] = useState(''); // holds the list of letters to be played back

  // function to be called each time a key is pressed. If a letter is passed as an argument, it will be stored in the logs state object.
  // since letters are only printed on the white keys, any time one is clicked the corresponding letter will be passed to the function.
  const handleChange = (letter) => {
    const newLogs = [...logs];
    newLogs.push(letter.toUpperCase());
    setLogs(newLogs);
  }

  // loop through the list of letters and trigger a function that will highlight the piano keys at 1 second intervals
  // a second function will also be triggered to run 1 second later so as to revert the changes made to highlight the keys
  const handlePlay = () => {
    let timeout1 = 1000;
    let timeout2 = 2000;

    letters.split(',').forEach(letter => {
      setTimeout(() => changeClassName(letter), timeout1);
      setTimeout(() => revertClassName(letter), timeout2);
      timeout1 += 1000;
      timeout2 += 1000;
    })
  }

  // since we are not simulating a click, we will utilize a clone class of the :active link CSS property
  const changeClassName = letter => {
    document.getElementById(`${id}-${letter}`).className = `white highlighted ${letter}`;
  }

  // restore the original CSS style of the li element of the piano keyboard
  const revertClassName = letter => {
    document.getElementById(`${id}-${letter}`).className = `white ${letter}`;
  }

  return (
    <div className='piano'>
      <ul>
        {/* Iterate through the piano key letters and render each key by calling the Keys component */}
        {pianoKeys.map((pianoKey, index) => {
          return (
            <Keys 
              key={`${id}-${index}`} 
              pianoKey={pianoKey} 
              handleChange={handleChange}
              id={id}
            />
          );
        })}
      </ul>
      <br />
      {/* Render the Logger component and pass it any letters stored in the logs state object */}
      <Logger logs={logs} />
      <br />
      <Player 
        handlePlay={handlePlay}
        handleChange={setLetters}
      />
    </div>
  );
}

export default Piano;