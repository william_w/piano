// Containers
import PianoLoader from './containers/PianoLoader';

export default [
  {
    path: '/',
    exact: true,
    component: PianoLoader,
  },
];