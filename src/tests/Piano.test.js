import React from 'react';
import { act } from 'react-dom/test-utils';
import { render, unmountComponentAtNode } from "react-dom";
import { mount } from "enzyme";
import expect from 'expect';

import Piano from '../components/Piano';

function setup() {

  const PianoWrapper = mount(<Piano />)

  return { PianoWrapper }
}

let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  // container *must* be attached to document so events work correctly.
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

describe('Piano component', () => {

  test('renders all the piano keys', () => {
    const { PianoWrapper } = setup();
    expect(PianoWrapper.find('li').at(0).text()).toEqual('C');
    expect(PianoWrapper.find('li').at(1).text()).toEqual('');
    expect(PianoWrapper.find('li').at(2).text()).toEqual('D');
    expect(PianoWrapper.find('li').at(3).text()).toEqual('');
    expect(PianoWrapper.find('li').at(4).text()).toEqual('E');
    expect(PianoWrapper.find('li').at(5).text()).toEqual('F');
    expect(PianoWrapper.find('li').at(6).text()).toEqual('');
    expect(PianoWrapper.find('li').at(7).text()).toEqual('G');
    expect(PianoWrapper.find('li').at(8).text()).toEqual('');
    expect(PianoWrapper.find('li').at(9).text()).toEqual('A');
    expect(PianoWrapper.find('li').at(10).text()).toEqual('');
    expect(PianoWrapper.find('li').at(11).text()).toEqual('B');
  });

  test('logs keys when they are clicked on', () => {
    act(() => {
      render(<Piano />, container);
    });

    const li1 = document.querySelector("[data-testid=li-c]");
    const li2 = document.querySelector("[data-testid=li-d]");
    const li3 = document.querySelector("[data-testid=li-e]");
    const li4 = document.querySelector("[data-testid=li-f]");
    const li5 = document.querySelector("[data-testid=li-g]");
    const li6 = document.querySelector("[data-testid=li-a]");
    const li7 = document.querySelector("[data-testid=li-b]");
    const loglbl = document.querySelector("[data-testid=log-label]");

    act(() => {
      li1.dispatchEvent(new MouseEvent("click", { bubbles: true }));
    });
    expect(loglbl.innerHTML).toBe('Log: C');

    act(() => {
      li2.dispatchEvent(new MouseEvent("click", { bubbles: true }));
    });
    expect(loglbl.innerHTML).toBe('Log: C D');

    act(() => {
      li3.dispatchEvent(new MouseEvent("click", { bubbles: true }));
    });
    expect(loglbl.innerHTML).toBe('Log: C D E');

    act(() => {
      li4.dispatchEvent(new MouseEvent("click", { bubbles: true }));
    });
    expect(loglbl.innerHTML).toBe('Log: C D E F');

    act(() => {
      li5.dispatchEvent(new MouseEvent("click", { bubbles: true }));
    });
    expect(loglbl.innerHTML).toBe('Log: C D E F G');

    act(() => {
      li6.dispatchEvent(new MouseEvent("click", { bubbles: true }));
    });
    expect(loglbl.innerHTML).toBe('Log: C D E F G A');

    act(() => {
      li7.dispatchEvent(new MouseEvent("click", { bubbles: true }));
    });
    expect(loglbl.innerHTML).toBe('Log: C D E F G A B');
  });

  test('plays a list of given letters', () => {
    const { PianoWrapper } = setup();

    act(() => {
      PianoWrapper.find('input').simulate('change', {target: {value: 'a,b,f,c,b,g,f'}});
    });

    expect(PianoWrapper.find('span').text()).toBe('');

    const e = { preventDefault: () => {} };
    const spy = jest.spyOn(e, 'preventDefault');
    act(() => {
      PianoWrapper.find('button').props().onClick(e);
    });

    expect(spy).toHaveBeenCalled();
  });

  test('fails to play due to invalid letters', () => {
    const { PianoWrapper } = setup();

    act(() => {
      PianoWrapper.find('input').simulate('change', {target: {value: 'a,3,b,f,c,b,g,f'}});
    });

    expect(PianoWrapper.find('span').text()).toBe('Please use small or capital letters [A to G] only.');
  });

  test('fails to play due to invalid format', () => {
    const { PianoWrapper } = setup();

    act(() => {
      PianoWrapper.find('input').simulate('change', {target: {value: 'a,bb,f,c,b,g,f'}});
    });

    expect(PianoWrapper.find('span').text()).toBe('Please type in only one letter seperated by a coma.');
  });

});