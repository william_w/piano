import React from 'react';
import renderer from 'react-test-renderer';
import expect from 'expect';

import PianoLoader from "../containers/PianoLoader";

describe('PianoLoader container', () => {

  test('renders correctly', () => {
    const tree = renderer
      .create(<PianoLoader />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});